import React from 'react';
import { nanoid } from "nanoid";
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

const INFO = [
  {id: "iten-"+nanoid(), name:"Comida", preco:"Preço1", description:"Esse é o teu lanche", images:""},
  {id: "iten-"+nanoid(), name:"Comidadois", preco:"Preço2", description:"Esse é o teu doce", images:""},
  {id: "iten-"+nanoid(), name:"Comidatres", preco:"Preço3", description:"Esse é o teu Suco", images:""}
]

ReactDOM.render(
  <React.StrictMode>
  <nav className="navbar navbar-dark bg-dark text-white">
    <h4>Casa dos Beduínos</h4>
  </nav>
    <App itens={INFO}/>
  </React.StrictMode>,
  document.getElementById('root')
);