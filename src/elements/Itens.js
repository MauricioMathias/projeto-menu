import React from 'react';


export default function Itens(props) {
    const editingTemplate = (
        <ul className="itens-list list-group">         
                    <li className="itens-inteiro list-group-item p-3 mb-2 text-white">
                        <div className="media">
                            <div className="media-body">
                                <h5 className="d-flex flex-row mt-0 mb-1" htmlFor={props.id}>{props.name}:</h5>
                                <p className="itens-description">{props.description}</p>
                                <button type="button" className="btn btn-warning" onClick={() =>props.deleteIten(props.id)}>
                                    Excluir <span className="iten-delete-name">{props.name}</span>
                                </button><br/>
                            </div>
                            <figure className="figure">
                            <img className="rounded float-right figure-img img-fluid" src={props.images} alt="" width="200" />
                            <figcaption className="figure-caption  text-right">R${props.preco}</figcaption>
                            </figure>
                        </div>
                    </li>
                </ul>
    );
    
    const doneTemplate = (
        <ul className="itens-list list-group">         
                    <li className="itens-inteiro list-group-item p-3 mb-2 text-white">
                        <div className="media">
                            <div className="media-body">
                                <h5 className="d-flex flex-row mt-0 mb-1" htmlFor={props.id}>{props.name}:</h5>
                                <p className="text itens-description">{props.description}</p>
                            </div>
                            <figure className="figure">
                            <img className="rounded float-right figure-img img-fluid" src={props.images} alt="" width="200" />
                            <figcaption className="figure-caption  text-right">R${props.preco}</figcaption>
                            </figure>
                        </div>
                    </li>
                </ul>
    );
    return(
        <div className="list-done">
        {props.estado ? editingTemplate : doneTemplate}
        </div>
    );
}