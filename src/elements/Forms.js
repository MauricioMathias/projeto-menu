import React, {useState} from 'react';
import ImageUploading from 'react-images-uploading';

export default function Form(props) { 
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [preco, setPreco] = useState('')
    const [images, setImages] = useState([]);
    const maxNumber = 1;
    const enviaImagem = (imageItem, addUpdateIndex) => {
        console.log(imageItem, addUpdateIndex);
        setImages(imageItem);
    }
    function resolveMudancaName(e){     
        setName(e.target.value);
    }
    function resolveMudancaDescription(e){     
        setDescription(e.target.value);
    }
    function resolveMudancaPreco(e){
        setPreco(e.target.value);
    }
    function resolveEnvio(e) {
        if(images[0] == null) {
            e.preventDefault();
            alert("Por favor, adicione uma foto!")
        } else{
        e.preventDefault();
        props.addItem(name, description, preco, images);
        setName("");
        setDescription("");
        setPreco("");
        setImages([]);
        }
    }
    return(
        <div className="input-form">
                <form className="input-form" onSubmit={resolveEnvio}>
                    <div className="form-group">
                        <label htmlFor="new-list-input" className="label-input">
                            O que deseja adicionar?
                        </label>
                    </div>
                    <div className="input-group mb-3">
                    <input
                        type="text"
                        id="new-list-name"
                        className="form-control input"
                        name="name"
                        placeholder="Nome do prato"
                        autoComplete="off"
                        value={name}
                        onChange={resolveMudancaName}
                    /><br/>
                    </div>
                    <div className="input-group mb-3">
                    <input
                        type="text"
                        id="new-list-description"
                        className="form-control input"
                        name="description"
                        placeholder="Escreva aqui uma descrição"
                        autoComplete="off"
                        value={description}
                        onChange={resolveMudancaDescription}
                    /><br/>
                    </div>
                    <div className="input-group mb-3">
                    <input
                        type="text"
                        id="new-list-preco"
                        className="form-control input"
                        name="preco"
                        placeholder="E o preço..."
                        autoComplete="off"
                        value={preco}
                        onChange={resolveMudancaPreco}
                    /><br/>
                    </div>
                    <button type="submit" className="button btn btn-info btn-group-vertical">Adicionar item</button>                     
                </form>
                <div className="image-up">
                    <ImageUploading
                        multiple
                        value={images}
                        onChange={enviaImagem}
                        maxNumber={maxNumber}
                        dataURLKey="data_url"
                    >
                        {({
                            imageList,
                            onImageUpload,
                            onImageRemove,
                            isDragging,
                            dragProps
                        }) => (
                            <div className="pre-imagem">
                                <button type="submit" className="button btn btn-info btn-group-vertical" style={isDragging ? { color: 'red' } : undefined}
                                onClick={onImageUpload}
                                {...dragProps}>
                                    Adicionar Imagem...
                                </button>  
                                {imageList.map((imageList, index) => (
                                    <div className="image-item">
                                        <br></br><img src={imageList['data_url']} alt="" width="100" />
                                        <button className="btn btn-info" onClick={() => onImageRemove(index)}>Remove</button>
                                    </div>                           
                                ))}
                            </div> 
                        )}
                    </ImageUploading>  
                </div> 
            <button type="button" className="button btn btn-info btn-group-vertical" onClick={() => props.esconderBotoes()}>
                Esconder
            </button>
        </div>
    );
}