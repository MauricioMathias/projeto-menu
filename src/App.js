import React, {useState} from 'react';
import { nanoid } from "nanoid";
import './App.css';
import Itens from './elements/Itens';
import Form from './elements/Forms';

function App(props) {
  const [estado, setEstado] = useState(true)
  var [itens, setItens] = useState(props.itens)
  function deleteIten(id){
    const remainingItens = itens.filter( iten => id !== iten.id);
    setItens(remainingItens)
  }
  function esconderBotoes(){
    if (estado === true) {
    return setEstado(false)
    } else {
      return setEstado(true)
    }
  }
  function addItem(name, description, preco, images){
    try{
      const newIten = { id:"iten-"+nanoid(), name: name, description: description, preco: preco, images: images[0].data_url};
      setItens([...itens, newIten])
      console.log(newIten)
    }
    catch(err) {
      alert("Desculpe, algo deu errado!")
    }
  }
  var itensList = itens.map(list => <Itens id={list.id} name={list.name} preco={list.preco} description={list.description} images={list.images} deleteIten={deleteIten} estado={estado}/>)
  return (
    <div className="container App">
      <div className="row">
        <div className="col-4">
          <Form addItem={addItem} esconderBotoes={esconderBotoes}/>
        </div>
        <div className="col full-list">
        {itensList}
        </div>
        <div className="col-1"></div>
      </div>
    </div>
  );
}

export default App;